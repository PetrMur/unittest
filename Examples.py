## @package Examples
#   Examples for tests

import numpy as np


set1 = {i * 2 for i in range(5)}
set2 = {0, 1, 2}
set3 = {7, 9}
set4 = set()
set5 = {11, 9}
set6 = {0}


bitVec1 = np.array([False, True, False, True])
bitVec2 = np.array([False, False, False, False])
bitVec3 = np.array([True, True, True, True])
bitVec4 = np.array([True, False, False])
bitVec5 = np.array([False, False])
bitVec6 = np.array([False, True, True, False])


matrix1 = np.array([[2, 3, 1],
                    [5, 2, 6],
                    [6, 1, 12]])

matrix2 = np.array([[2, 3, 1, 2],
                    [5, 2, 6, 4],
                    [6, 1, 12, 6],
                    [7, 2, 4, 2]])

matrix3 = np.array([[2, 3],
                    [5, 2]])

matrix4 = np.array([[-2, -3, -1],
                    [-5, -2, -6],
                    [-6, -1, -12]])

matrix5 = np.array([[6, 2, 3],
                    [1, 8, 7]])

matrix6 = np.array([[0, 0, 0],
                    [0, 0, 0],
                    [0, 0, 0]])