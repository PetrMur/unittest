## @package ParamTest
#   Example parameterized test in "unittest"


import unittest
import paramunittest
from Examples import *


@paramunittest.parametrized(
    (set1, set2, {4, 6, 8}, {0, 1, 2, 4, 6, 8}, {0, 2}),
    (set5, set1, {9, 11}, {0, 2, 4, 6, 8, 9, 11}, set4),
    (set2, set6, {1, 2}, set2, {0}),
    (set3, set4, set3, set3, set4)
)
class TestSet(unittest.TestCase):
    def setParameters(self, param1, param2, paramDif, paramUnion, paramIntersect):
        """ """
        self.param1 = param1
        self.param2 = param2
        self.paramDif = paramDif
        self.paramUnion = paramUnion
        self.paramIntersect = paramIntersect

    def test_issubset(self):
        """ Проверка на включение в данное множество """
        self.assertFalse(self.param1.issubset(self.param2))

    def test_setDif(self):
        """ Разность множест """
        self.assertEqual(self.param1.difference(self.param2), self.paramDif)

    def test_setEqual(self):
        """ Сравнение множеств """
        self.assertFalse(set1 == set2)
        self.assertFalse(set2 == set6)
        self.assertTrue(set2 == set2)
        self.assertFalse(set2 == set1)

    def test_union(self):
        """ Объеденение множеств """
        self.assertEqual(set.union(self.param1, self.param2), self.paramUnion)

    def test_intersect(self):
        """ Пересечение множеств """
        self.assertEqual(set.intersection(self.param1, self.param2), self.paramIntersect)


if __name__ == '__main__':
    unittest.main()
