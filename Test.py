## @package Test
#   Examples for "unittest" tests


import unittest
from fractions import Fraction
from Examples import *


class TestVec(unittest.TestCase):
    def test_vecSum(self):
        """ Сумма векторов """
        self.assertTrue(np.array_equal(np.array([1, 3]) + np.array([0, 3]), np.array([1, 6])))
        self.assertTrue(np.array_equal(np.array([-3, -4]) + np.array([4, 2]), np.array([1, -2])))
        self.assertTrue(np.array_equal(np.array([0, 0]) + np.array([6, 8]), np.array([6, 8])))
        self.assertFalse(np.array_equal(np.array([-1, -2]) + np.array([2, 1]), np.array([3, 3])))

    def test_vecDif(self):
        """ Разность векторов """
        self.assertTrue(np.array_equal(np.array([1, 3]) - np.array([0, 3]), np.array([1, 0])))
        self.assertTrue(np.array_equal(np.array([3, 0]) - np.array([-4, -2]), np.array([7, 2])))
        self.assertTrue(np.array_equal(np.array([0, 0]) - np.array([6, 8]), np.array([-6, -8])))
        self.assertFalse(np.array_equal(np.array([1, 2]) - np.array([-2, -1]), np.array([-1, 1])))

    def test_vecEqual(self):
        """ Сравнение векторов """
        self.assertFalse(np.array_equal(np.array([1, 3]), np.array([1, 0])))
        self.assertFalse(np.array_equal(np.array([1, 2]), np.array([-1, -2])))
        self.assertTrue(np.array_equal(np.array([10, 12]), np.array([10, 12])))
        self.assertFalse(np.array_equal(np.array([2, 2]), np.array([2, 2, 0])))

    def test_vecScalarMulti(self):
        """ Скалярное призведение """
        self.assertTrue(np.array_equal(np.sum(np.array([1, 3]) * np.array([0, 3])), 9))
        self.assertTrue(np.array_equal(np.sum(np.array([4, 1]) * np.array([3, 5])), 17))
        self.assertTrue(np.array_equal(np.sum(np.array([1, 0]) * np.array([0, 3])), 0))
        self.assertFalse(np.array_equal(np.sum(np.array([3, 2]) * np.array([0, 3])), 0))

    def test_vecLen(self):
        """ Длина вектора """
        self.assertTrue(np.array_equal(np.linalg.norm([0, 3]), 3))
        self.assertTrue(np.array_equal(np.linalg.norm([3, 3]), 4.242640687119285))
        self.assertTrue(np.array_equal(np.linalg.norm([-4, -3]), 5))
        self.assertFalse(np.array_equal(np.linalg.norm([2, 12]), 3))


class TestRatNum(unittest.TestCase):
    def test_ratSum(self):
        """ Сложение рациональных чисел """
        self.assertEqual(Fraction(1, 3) + Fraction(1, 7), Fraction(10, 21))
        self.assertEqual(Fraction(-1, 4) + Fraction(1, 7), Fraction(-3, 28))
        self.assertEqual(Fraction(12, 13) + Fraction(5, 6), Fraction(137, 78))
        self.assertEqual(Fraction(1, 3) + Fraction(-1, 3), 0)

    def test_ratDif(self):
        """ Разность рациональных чисел """
        self.assertEqual(Fraction(1, 3) - Fraction(1, 7), Fraction(4, 21))
        self.assertEqual(Fraction(-1, 4) - Fraction(1, 7), Fraction(-11, 28))
        self.assertEqual(Fraction(12, 13) - Fraction(0, 6), Fraction(12, 13))
        self.assertEqual(Fraction(1, 3) - Fraction(1, 3), 0)

    def test_ratEqual(self):
        """ Сравнение рациональных чисел """
        self.assertFalse(Fraction(1, 2) == Fraction(3, 5))
        self.assertFalse(Fraction(5, 7) == Fraction(4, 7))
        self.assertTrue(Fraction(2, 3) == Fraction(4, 6))
        self.assertFalse(Fraction(-1, 5) == Fraction(1, 5))

    def test_ratMulti(self):
        """ Умножение рациональных чисел """
        self.assertEqual(Fraction(5, 6) * Fraction(1, 2), Fraction(5, 12))
        self.assertEqual(Fraction(1, 2) * 3, Fraction(3, 2))
        self.assertEqual(Fraction(5, 13) * Fraction(-3, 23), Fraction(-15, 299))
        self.assertEqual(Fraction(6, 1) * Fraction(3, 21), Fraction(18, 21))

    def test_ratDiv(self):
        """ Деление рациональных чисел """
        self.assertEqual(Fraction(5, 6) / Fraction(1, 2), Fraction(10, 6))
        self.assertEqual(Fraction(1, 2) / 3, Fraction(1, 6))
        self.assertEqual(Fraction(5, 13) / Fraction(-3, 23), Fraction(-115, 39))
        self.assertEqual(Fraction(6, 1) / Fraction(3, 21), Fraction(126, 3))


class TestBit(unittest.TestCase):
    def test_or(self):
        """ Логическая операция 'или' """
        self.assertTrue((bitVec1 | bitVec2 == bitVec1).all())
        self.assertTrue((bitVec2 | bitVec3 == bitVec3).all())
        with self.assertRaises(ValueError):
            bitVec5 | bitVec4

    def test_and(self):
        """ Логическая операция 'и' """
        self.assertTrue((bitVec6 & bitVec3 == np.array([False, True, True, False])).all())
        self.assertTrue((bitVec2 & bitVec3 == np.array([False, False, False, False])).all())
        self.assertTrue((bitVec1 & bitVec6 == np.array([False, True, False, False])).all())
        with self.assertRaises(ValueError):
            bitVec5 & bitVec3

    def test_betVecEqual(self):
        """ Сравнение """
        self.assertFalse((bitVec1 == bitVec6).all())
        self.assertTrue((bitVec3 == np.array([True, True, True, True])).all())
        self.assertFalse((bitVec1 == bitVec2).all())
        self.assertFalse(bitVec1 == bitVec5)

    def test_not(self):
        """ Логическая операция 'не' """
        self.assertTrue((~bitVec1 == np.array([True, False, True, False])).all())
        self.assertTrue((~bitVec2 == np.array([True, True, True, True])).all())
        self.assertTrue((~bitVec4 == np.array([False, True, True])).all())
        self.assertTrue((~bitVec5 == np.array([True, True])).all())

    def test_implication(self):
        """ Импликация """
        self.assertTrue((~bitVec1 | bitVec6 == np.array([True, True, True, False])).all())
        self.assertTrue((~bitVec6 | bitVec1 == np.array([True, True, False, True])).all())
        self.assertTrue((~bitVec2 | bitVec3 == np.array([True, True, True, True])).all())
        with self.assertRaises(ValueError):
            ~bitVec5 | bitVec3


class TestMatrix(unittest.TestCase):
    def test_matSum(self):
        """ Сумма матриц """
        self.assertTrue(np.array_equal(matrix1 + matrix4, matrix6))

        with self.assertRaises(ValueError):
            matrix1 + matrix3

        with self.assertRaises(ValueError):
            matrix2 + matrix1

    def test_matScalarMulti(self):
        """ Произведение матрицы на скаляр """
        self.assertTrue(np.array_equal(matrix1 * 2, np.array([[4, 6, 2],
                                                              [10, 4, 12],
                                                              [12, 2, 24]])))

        self.assertTrue(np.array_equal(matrix3 * 0, np.array([[0, 0],
                                                              [0, 0]])))

        self.assertTrue(np.array_equal(matrix5 * 1.5, np.array([[9.0, 3.0, 4.5],
                                                                [1.5, 12.0, 10.5]])))

        self.assertFalse(np.array_equal(matrix6 * 1, np.array([[1, 1, 1],
                                                               [1, 1, 1],
                                                               [1, 1, 1]])))

    def test_matEqual(self):
        """ Сравнение матриц"""
        self.assertFalse(np.array_equal(matrix1, matrix4))
        self.assertFalse(np.array_equal(matrix1, matrix3))
        self.assertTrue(np.array_equal(matrix6, matrix6))
        self.assertFalse(np.array_equal(matrix1, matrix6))

    def test_matMulti(self):
        """ Произведение матриц """
        self.assertTrue(np.array_equal(matrix1 * matrix6, np.array([[0, 0, 0],
                                                                    [0, 0, 0],
                                                                    [0, 0, 0]])))

        with self.assertRaises(ValueError):
            matrix5 * matrix4

        with self.assertRaises(ValueError):
            matrix3 + matrix5

        self.assertFalse(np.array_equal(matrix4 * matrix6, matrix4))

    def test_tr(self):
        """ Транспонирование """
        self.assertTrue(np.array_equal(matrix6.transpose(), matrix6))
        self.assertTrue(np.array_equal(matrix1.transpose(), np.array([[2, 5, 6],
                                                                      [3, 2, 1],
                                                                      [1, 6, 12]])))

        self.assertTrue(np.array_equal(matrix5.transpose(), np.array([[6, 1],
                                                                      [2, 8],
                                                                      [3, 7]])))

        self.assertFalse(np.array_equal(matrix3.transpose(), np.array([[2, 3],
                                                                       [2, 5]])))


if __name__ == '__main__':
    unittest.main()
